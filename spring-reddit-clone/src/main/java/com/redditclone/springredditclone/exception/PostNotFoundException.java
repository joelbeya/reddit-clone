package com.redditclone.springredditclone.exception;

public class PostNotFoundException extends RuntimeException {
    public PostNotFoundException(String string) {
        super(string);
    }
}
