package com.redditclone.springredditclone.exception;

public class SpringRedditCloneException extends RuntimeException {
    public SpringRedditCloneException(String customException) {
        super(customException);
    }

    public SpringRedditCloneException(String customException, Exception exception) {
        super(customException, exception);
    }
}
