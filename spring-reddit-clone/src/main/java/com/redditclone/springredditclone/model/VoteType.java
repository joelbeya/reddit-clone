package com.redditclone.springredditclone.model;

import com.redditclone.springredditclone.exception.SpringRedditCloneException;

import java.util.Arrays;

public enum VoteType {
    UPVOTE(1), DOWNVOTE(-1),;

    private Integer direction;

    VoteType(int direction) {
    }

    public Integer getDirection() { return direction; }

    private static VoteType lookup(Integer direction) {
        return Arrays.stream(VoteType.values())
                .filter(voteType -> voteType.getDirection().equals(direction))
                .findAny()
                .orElseThrow(() -> new SpringRedditCloneException("Vote not found"));
    }
}
